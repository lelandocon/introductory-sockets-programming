#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <iostream>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define SERVER_A_PORT	21760
#define SERVER_B_PORT	22760
#define AWS_UDP_PORT	23760
#define AWS_TCP_PORT	24760
#define AWS_TCP_PORT	24760
#define localhostIP		"127.0.0.1"
#define MAXBUFLEN		2048
#define interlim		"\n"
#define intralim		":"
#define vertexlim		")"
#define linelim			","
#define propHead		"propagation speed"
#define transHead		"transmission speed"
#define fileHead		"file size"
#define propDelayHead	"prop delay"
#define transDelayHead	"trans delay"
#define totalDelayHead	"total delay"
#define distanceHead	"distance"