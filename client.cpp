#include "warmsocks.h"

void printFinalResults(char * messageFromAWS)
{
	/* I'm writing this function in a brittle way, in that
	it will depend on the order of the components in the message*/
	char **wholeMessagePtr=&messageFromAWS;
	/* PLAN: print header*/
	printf("The client has received results from AWS:\n");
	printf("----------------------------------------------------------------------\n");
	printf("Destination\tMin Length\tTt\t\tTp\t\tDelay\n");
	printf("----------------------------------------------------------------------\n");
	/* PLAN: use strtok_r to split lines*/
	char *line=strtok_r(messageFromAWS, interlim, wholeMessagePtr);
	while(line!=NULL)
	{
		char** separatingVertEtc=&line;
		char * vertex=strtok_r(line, vertexlim,separatingVertEtc);
		char * restOfLine=strtok_r(line, vertexlim,separatingVertEtc);

		char ** separatingComps=&restOfLine;
		char * distance=strtok_r(restOfLine, linelim, separatingComps);
		char * trans=strtok_r(restOfLine, linelim, separatingComps);
		char * prop=strtok_r(restOfLine, linelim, separatingComps);
		char * delay=strtok_r(restOfLine, linelim, separatingComps);

		char * distanceStart=strtok(distance, intralim);
		char * distanceValue=strtok(NULL, intralim);

		char * transStart=strtok(trans, intralim);
		char * transValue=strtok(NULL, intralim);
		float  transFloat=atof(transValue);

		char * propStart=strtok(prop, intralim);
		char * propValue=strtok(NULL, intralim);
		float  propFloat=atof(propValue);

		char * delayStart=strtok(delay, intralim);
		char * delayValue=strtok(NULL, intralim);
		float  delayFloat=atof(delayValue);

		printf("%s\t\t",vertex);
		printf("%s\t\t",distanceValue);
		printf("%.2f\t\t",transFloat);
		printf("%.2f\t\t",propFloat);
		printf("%.2f\n",delayFloat);

		line=strtok_r(messageFromAWS, interlim, wholeMessagePtr);		
	}
	/* PLAN: print trailer*/
	printf("----------------------------------------------------------------------\n");
}
int main(int argc, char * argv[]){
	int sockfd, numbytes;
    char buf[MAXBUFLEN];

    /* PLAN: collect commandline arguments*/
    if (argc!=4){
    	fprintf(stderr, "Usage: [Map ID] [Source Vertex Index] [File Size]");
    }

    std::string mapID (argv[1]);
    std::string sourceVertexIndex (argv[2]);
    std::string fileSize (argv[3]);
    std::string completeMessage;
    completeMessage=mapID+","+sourceVertexIndex+","+fileSize;

    printf("The client is up and running.\n");

    /* PLAN: figure out the address info of the AWS server
	with getaddrinfo or by filling out the address manually*/
    /*hardcoding the AWS TCP socket address below*/
    struct sockaddr_in aws_tcp;
	aws_tcp.sin_family = AF_INET;
	aws_tcp.sin_port=htons(AWS_TCP_PORT);
	aws_tcp.sin_addr.s_addr=inet_addr(localhostIP);
    memset(aws_tcp.sin_zero, '\0', sizeof aws_tcp);

    /* PLAN: create socket with information about the domain, type,
	and protocol of the AWS server address*/
    if ((sockfd = socket(PF_INET, SOCK_STREAM,
            0)) == -1) {
        perror("client: socket");
        exit(1);
    }
	/* PLAN: connect to the AWS server with the newly created socket*/
	if (connect(sockfd, (struct sockaddr *) &aws_tcp, sizeof aws_tcp) == -1) {
        close(sockfd);
        perror("client: connect");
        exit(1);
    }


	/* PLAN: send message with collected commandline parameters in
	specific format*/
	if ((numbytes=send(sockfd, completeMessage.c_str(), completeMessage.length()
		, 0)) == -1){
		perror("client: send");
		exit(1);
	}
	struct sockaddr_in myAddr;
	socklen_t myLen=sizeof myAddr;
	getsockname(sockfd, (struct sockaddr*) &myAddr, &myLen);
	printf("The client has sent query to AWS using TCP over port %d: ",ntohs(myAddr.sin_port));
	printf("start vertex %s; map %s; file size %s.\n",argv[2],argv[1],argv[3]);

	/* PLAN: receive (i.e. wait for) message from AWS server*/

    if ((numbytes = recv(sockfd, buf, MAXBUFLEN, 0)) == -1) {
        perror("client:recv");
        exit(1);
    }

    buf[numbytes] = '\0';

    char * messageFromAWS=buf;

    //printf("client: received '%s'\n",buf);

    printFinalResults(messageFromAWS);

    /* PLAN: close the socket*/

    close(sockfd);

    return 0;
}
