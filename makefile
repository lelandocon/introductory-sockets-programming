all: client aws.o serverA.o serverB.o

client: client.cpp
	g++ -o client client.cpp

aws.o: aws.cpp
	g++ -o aws.o aws.cpp

serverA.o: serverA.cpp dijkstra.cpp
	g++ -o serverA.o serverA.cpp
	
serverB.o: serverB.cpp
	g++ -o serverB.o serverB.cpp

client, aws.o, serverA.o, serverB.o: warmsocks.h

serverA: serverA.o
	./serverA.o

serverB: serverB.o
	./serverB.o

aws: aws.o
	./aws.o

clean:
	rm -f *.o client
