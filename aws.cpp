#include <sys/wait.h>
#include <signal.h>
#include "warmsocks.h"

#define BACKLOG 10   // how many pending connections queue will hold


void printResultsFromA(char * message)
{
	/* This function is very similar to parseMessageFromAWS in serverB.cpp*/
	/*PLAN: print header*/
	printf("The AWS has received shortest path from server A:\n"
		"--------------------------------\n"
		"Destination\tMin Length\n"
		"--------------------------------\n");

	/*PLAN: use strtok_r to go through results*/
	char **saveptrTok=&message;
	char * token=strtok_r(message, interlim, saveptrTok);
	while(token!=NULL)
	{
		char **saveptrSub=&token;
		/*PLAN: take the first half of the token, check its type*/
		char* firstHalf= strtok_r(token, intralim, saveptrSub);
		char* secondHalf=strtok_r(token, intralim, saveptrSub);
		if((atof(firstHalf)==0) and not (strcmp(firstHalf, "0")==0))//not a number
		{
			int what=1; //this is a dummy statement
		}else{//first half is a number, and thus a vertex
			int vertex=atoi(firstHalf);
			int distance=atoi(secondHalf);
			printf("%d\t\t%d\n", vertex, distance);
		}
		token=strtok_r(message, interlim, saveptrTok);
	}
	/*PLAN: print trailer*/
	printf("--------------------------------\n");
}

void printDelayResults(char * message)
{
	/*This function is very similar to printFinalResults in client.cpp*/
		/* I'm writing this function in a brittle way, in that
	it will depend on the order of the components in the message*/
	char **wholeMessagePtr=&message;
	/* PLAN: print header*/
	printf("The client has received results from AWS:\n");
	printf("----------------------------------------------------------------------\n");
	printf("Destination\tTt\t\tTp\t\tDelay\n");
	printf("----------------------------------------------------------------------\n");
	/* PLAN: use strtok_r to split lines*/
	char *line=strtok_r(message, interlim, wholeMessagePtr);
	while(line!=NULL)
	{
		char** separatingVertEtc=&line;
		char * vertex=strtok_r(line, vertexlim,separatingVertEtc);
		char * restOfLine=strtok_r(line, vertexlim,separatingVertEtc);

		char ** separatingComps=&restOfLine;
		char * distance=strtok_r(restOfLine, linelim, separatingComps);
		char * trans=strtok_r(restOfLine, linelim, separatingComps);
		char * prop=strtok_r(restOfLine, linelim, separatingComps);
		char * delay=strtok_r(restOfLine, linelim, separatingComps);

		char * distanceStart=strtok(distance, intralim);
		char * distanceValue=strtok(NULL, intralim);

		char * transStart=strtok(trans, intralim);
		char * transValue=strtok(NULL, intralim);
		float  transFloat=atof(transValue);

		char * propStart=strtok(prop, intralim);
		char * propValue=strtok(NULL, intralim);
		float  propFloat=atof(propValue);

		char * delayStart=strtok(delay, intralim);
		char * delayValue=strtok(NULL, intralim);
		float  delayFloat=atof(delayValue);

		printf("%s\t\t",vertex);
		//printf("%s\t\t",distanceValue);
		printf("%.2f\t\t",transFloat);
		printf("%.2f\t\t",propFloat);
		printf("%.2f\n",delayFloat);

		line=strtok_r(message, interlim, wholeMessagePtr);		
	}
	/* PLAN: print trailer*/
	printf("----------------------------------------------------------------------\n");
}
/*GRADERS: sigchld_handler and get_in_addr are from Beej's guide*/

void sigchld_handler(int s)
{
    // waitpid() might overwrite errno, so we save and restore it:
    int saved_errno = errno;

    while(waitpid(-1, NULL, WNOHANG) > 0);

    errno = saved_errno;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(void){
	int sockfd, new_fd, aws_udp_fd;  // listen on sock_fd, new connection on new_fd
    struct sockaddr_storage their_tcp_addr, their_udp_addr; // connector's address information
    socklen_t their_tcp_addr_len, their_udp_addr_len;
    struct sigaction sa;
    int yes=1;
    char s[INET6_ADDRSTRLEN];
    int numBytesSent, numBytesReceived;
    char buf[MAXBUFLEN];

    /* PLAN: get address info for AWS TCP port*/
    /*hardcoding the AWS TCP socket address below*/
    struct sockaddr_in aws_tcp;
	aws_tcp.sin_family = AF_INET;
	aws_tcp.sin_port=htons(AWS_TCP_PORT);
	aws_tcp.sin_addr.s_addr=inet_addr(localhostIP);
    memset(aws_tcp.sin_zero, '\0', sizeof aws_tcp);

    /*hardcoding the AWS UDP socket address*/
    struct sockaddr_in aws_udp;
	aws_udp.sin_family = AF_INET;
	aws_udp.sin_port=htons(AWS_UDP_PORT);
	aws_udp.sin_addr.s_addr=inet_addr(localhostIP);
    memset(aws_udp.sin_zero, '\0', sizeof aws_udp);

    /*hardcoding server A UDP socket address*/
    struct sockaddr_in serverA;
	serverA.sin_family = AF_INET;
	serverA.sin_port=htons(SERVER_A_PORT);
	serverA.sin_addr.s_addr=inet_addr(localhostIP);
    memset(serverA.sin_zero, '\0', sizeof serverA);

    /*hardcoding server B UDP socket address*/
    struct sockaddr_in serverB;
	serverB.sin_family = AF_INET;
	serverB.sin_port=htons(SERVER_B_PORT);
	serverB.sin_addr.s_addr=inet_addr(localhostIP);
    memset(serverB.sin_zero, '\0', sizeof serverB);

    /* PLAN: create a parent socket for TCP*/
    if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
        perror("server: socket");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
            sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }

    /* PLAN: bind that socket to the AWS TCP port number */	
    if (bind(sockfd, (struct sockaddr *) &aws_tcp, sizeof aws_tcp) == -1) {
        close(sockfd);
        perror("server: bind");
        exit(1);
    }

    /* PLAN: start listening for client messages*/
    if (listen(sockfd, BACKLOG) == -1) {
        perror("listen");
        exit(1);
    }

    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(1);
    }

    printf("The AWS is up and running.\n");
    //printf("server: waiting for connections...\n");

    /* PLAN: begin infinite loop*/

    while(1) {  // main accept() loop

    	/* PLAN: block on accept messages from client*/
    	their_tcp_addr_len = sizeof their_tcp_addr;
        new_fd = accept(sockfd, (struct sockaddr *)&their_tcp_addr, &their_tcp_addr_len);
        if (new_fd == -1) {
            perror("accept");
            continue;
        }

        inet_ntop(their_tcp_addr.ss_family,
            get_in_addr((struct sockaddr *)&their_tcp_addr),
            s, sizeof s);
        //printf("server: got connection from %s\n", s);

        if (!fork()) { // this is the child process
            close(sockfd); // child doesn't need the listener

            /* PLAN: receive message from client via new TCP connection*/
            if ((numBytesReceived = recv(new_fd, buf, MAXBUFLEN-1, 0)) == -1) {
		        perror("aws tcp:recv");
		        exit(1);
		    }

		    buf[numBytesReceived] = '\0';

		    /* PLAN: extract file size from the client message*/
		    const char * delim=",";
		    char * duplicateMessage=strdup(buf);
		    char * mapID=strtok(duplicateMessage, delim);
		    char * sourceVertex=strtok(NULL, delim);
		    char * fileSize=strtok(NULL, delim);

		    printf("The AWS has received map ID %s, start vertex %s ", mapID, sourceVertex);
		    printf("and file size %s from the client using TCP over port %d\n",fileSize, ntohs(aws_tcp.sin_port));

		    /*
		    printf("listener: got packet from %s\n",
		        inet_ntop(their_udp_addr.ss_family,
		            get_in_addr((struct sockaddr *)&their_udp_addr),
		            s, sizeof s));
		    printf("listener: packet is %d bytes long\n", numBytesReceived);
		    printf("listener: packet contains \"%s\"\n", buf);
		    */


            /*execute server A and server B interactions in here*/

            /* PLAN: after receiving message from client, send datagram to server A*/
            if ((aws_udp_fd = socket(PF_INET, SOCK_DGRAM,
                getprotobyname("udp")->p_proto)) == -1) {
	            perror("aws udp: socket");
	            exit(1);
	        }

	        if (bind(aws_udp_fd, (struct sockaddr *) &aws_udp, sizeof aws_udp) == -1) {
	            close(aws_udp_fd);
	            perror("aws udp: bind");
	            exit(1);
	        }

	        const char * messageToA = buf;

	        if(numBytesSent=sendto(aws_udp_fd, messageToA, strlen(messageToA), 0,
	        	(struct sockaddr *)&serverA, sizeof serverA)==-1){
	        	perror("aws udp: sendto serverA");
	        	exit(1);
	        }

	        printf("The AWS has sent map ID and starting vertex to server A "
	        	"using UDP over port %d\n", ntohs(aws_udp.sin_port));

	        /* PLAN: receive datagram from server A of shortest paths*/
	        their_udp_addr_len=sizeof their_udp_addr;
	        if ((numBytesReceived = recvfrom(aws_udp_fd, buf, MAXBUFLEN-1 , 0,
		        (struct sockaddr *)&their_udp_addr, &their_udp_addr_len)) == -1) {
		        perror("aws udp: recvfrom serverA");
		        exit(1);
		    }
		    buf[numBytesReceived] = '\0';
		    char * messageFromA=strdup(buf);
		    printResultsFromA(messageFromA);

		    /*
		    printf("listener: got packet from %s\n",
		        inet_ntop(their_udp_addr.ss_family,
		            get_in_addr((struct sockaddr *)&their_udp_addr),
		            s, sizeof s));
		    printf("listener: packet is %d bytes long\n", numBytesReceived);
		    printf("listener: packet contains \"%s\"\n", buf);
		    */

		    /* interact with server B*/

		    /* PLAN: send datagram to server B*/

		    //const char * messageToB = "How about you, B?";
		    std::string messageToB (buf);
		    messageToB=messageToB+interlim+fileHead+intralim+fileSize;


		    if(numBytesSent=sendto(aws_udp_fd, messageToB.c_str(), 
		    	strlen(messageToB.c_str()), 0,
	        	(struct sockaddr *)&serverB, sizeof serverB)==-1){
	        	perror("aws udp: sendto serverA");
	        	exit(1);
	        }

	        printf("The AWS has sent path length, propagation speed "
	        	"and transmission speed to server B using UDP "
	        	"over port %d.\n", ntohs(aws_udp.sin_port));

	        /* PLAN: receive datagram from server B*/

	        their_udp_addr_len=sizeof their_udp_addr;
	        if ((numBytesReceived = recvfrom(aws_udp_fd, buf, MAXBUFLEN-1 , 0,
		        (struct sockaddr *)&their_udp_addr, &their_udp_addr_len)) == -1) {
		        perror("aws udp: recvfrom serverB");
		        exit(1);
		    }

		    buf[numBytesReceived] = '\0';
		    char * messageFromB=strdup(buf);
		    printDelayResults(messageFromB);

		    /*
		    printf("listener: got packet from %s\n",
		        inet_ntop(their_udp_addr.ss_family,
		            get_in_addr((struct sockaddr *)&their_udp_addr),
		            s, sizeof s));
		    printf("listener: packet is %d bytes long\n", numBytesReceived);
		    printf("listener: packet contains \"%s\"\n", buf);
		    */

		    char * finalResultsForClient=buf;

		    /* PLAN: finally, send results to client and go back to start of infinite loop*/
            if ((numBytesSent=send(new_fd, finalResultsForClient,
            	strlen(finalResultsForClient), 0)) == -1)
                perror("send");

            printf("The AWS has sent calculated delay to client using "
            	"TCP over port %d.\n", ntohs(aws_tcp.sin_port));

            close(aws_udp_fd);
            close(new_fd);
            exit(0);
        }
        close(new_fd);  // parent doesn't need this
    }

    return 0;
}
