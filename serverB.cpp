#include "warmsocks.h"
#include<bits/stdc++.h>
#include <sstream>
using namespace std;

/* GRADERS: I got this rounding function from the link below:
https://www.geeksforgeeks.org/rounding-floating-point-number-two-decimal-places-c-c/*/

float round(float var) 
{ 
    // 37.66666 * 100 =3766.66 
    // 3766.66 + .5 =3767.16    for rounding off value 
    // then type cast to int so value is 3767 
    // then divided by 100 so the value converted into 37.67 
    float value = (int)(var * 100 + .5); 
    return (float)value / 100; 
}

string convertNumToString(float f)
{
	ostringstream numToString;
	numToString<<f;
	return numToString.str();
}


string calculateDelays(double* propP, double* transP, 
	double* fileP, map<int,int> *distP)
{
	string message; //this will be what server B sends to AWS
	/*PLAN: iterate over map*/
	map<int, int>::iterator it;
	for(it = distP->begin(); it!=distP->end(); ++it)
	{
		int vertex=it->first;
		int distance=it->second;
		/*PLAN: calculate the propagation and transmission delays*/
		double fileSizeInBytes=*fileP/8.0;
		double transmissionDelay=fileSizeInBytes/(*transP);
		double propagationDelay=distance/(*propP);
		double totalDelay=transmissionDelay+propagationDelay;

		/*PLAN: take only the first two decimals of each quantity*/
		float roundTrans=roundf(transmissionDelay*100)/100;
		float roundProp=roundf(propagationDelay*100)/100;
		float roundTotal=roundf(totalDelay*100)/100;
		/*PLAN: make a string out of these elements, and add them to the message
		format will be "vertexID)distanceHead:distance,propDelayHead:propDelay..." and so on*/
		string line;
		line=convertNumToString(vertex)+vertexlim;
		line=line+distanceHead+intralim+convertNumToString(distance)+linelim;
		line=line+transDelayHead+intralim+convertNumToString(roundTrans)+linelim;
		line=line+propDelayHead+intralim+convertNumToString(roundProp)+linelim;
		line=line+totalDelayHead+intralim+convertNumToString(roundTotal)+interlim;
		message=message+line;
	}
	return message;
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void parseMessageFromAWS(char * message, double* propP, 
	double* transP, double* fileP, map<int,int> *distP)
{
	char **saveptrTok=&message;
	char * token=strtok_r(message, interlim, saveptrTok);
	while(token!=NULL)
	{
		char **saveptrSub=&token;
		/*PLAN: take the first half of the token, check its type*/
		char* firstHalf= strtok_r(token, intralim, saveptrSub);
		char* secondHalf=strtok_r(token, intralim, saveptrSub);
		if((atof(firstHalf)==0) and not (strcmp(firstHalf, "0")==0))//not a number
		{
			if(strcmp(firstHalf, propHead)==0)
			{
				*propP=atof(secondHalf);
			}
			if(strcmp(firstHalf, transHead)==0)
			{
				*transP=atof(secondHalf);
			}
			if(strcmp(firstHalf, fileHead)==0)
			{
				*fileP=atof(secondHalf);
			}
		}else{//first half is a number, and thus a vertex
			int vertex=atoi(firstHalf);
			int distance=atoi(secondHalf);
			distP->insert(make_pair(vertex, distance));
		}
		token=strtok_r(message, interlim, saveptrTok);
	}
}

void printParsedMessage(double* propP, 
	double* transP, double* fileP, map<int,int> *distP)
{
	/* PLAN: print header*/
	printf("The Server B has received data for calculation:\n");
	/* PLAN: print speeds*/
	printf("* Propagation speed: %.2f km/s;\n",*propP);
	printf("* Transmission speed: %.2f Bytes/s;\n", *transP);
	/* PLAN: iterate over map*/
	map<int, int>::iterator it;
	for(it = distP->begin(); it!=distP->end(); ++it)
	{
		int vertex=it->first;
		int distance=it->second;
		printf("* Path length for destination %d: %d\n", vertex, distance);
	}
}

void printTotalDelay(char * message)
{
	/*This function is very similar to printFinalResults in client.cpp*/
		/* I'm writing this function in a brittle way, in that
	it will depend on the order of the components in the message*/
	char **wholeMessagePtr=&message;
	/* PLAN: print header*/
	printf("The Server B has finished the calculation of the delays:\n");
	printf("----------------------------------\n");
	printf("Destination\tDelay\n");
	printf("----------------------------------\n");
	/* PLAN: use strtok_r to split lines*/
	char *line=strtok_r(message, interlim, wholeMessagePtr);
	while(line!=NULL)
	{
		char** separatingVertEtc=&line;
		char * vertex=strtok_r(line, vertexlim,separatingVertEtc);
		char * restOfLine=strtok_r(line, vertexlim,separatingVertEtc);

		char ** separatingComps=&restOfLine;
		char * distance=strtok_r(restOfLine, linelim, separatingComps);
		char * trans=strtok_r(restOfLine, linelim, separatingComps);
		char * prop=strtok_r(restOfLine, linelim, separatingComps);
		char * delay=strtok_r(restOfLine, linelim, separatingComps);

		char * distanceStart=strtok(distance, intralim);
		char * distanceValue=strtok(NULL, intralim);

		char * transStart=strtok(trans, intralim);
		char * transValue=strtok(NULL, intralim);
		float  transFloat=atof(transValue);

		char * propStart=strtok(prop, intralim);
		char * propValue=strtok(NULL, intralim);
		float  propFloat=atof(propValue);

		char * delayStart=strtok(delay, intralim);
		char * delayValue=strtok(NULL, intralim);
		float  delayFloat=atof(delayValue);

		printf("%s\t\t",vertex);
		//printf("%s\t\t",distanceValue);
		//printf("%.2f\t\t",transFloat);
		//printf("%.2f\t\t",propFloat);
		printf("%.2f\n",delayFloat);

		line=strtok_r(message, interlim, wholeMessagePtr);		
	}
	/* PLAN: print trailer*/
	printf("----------------------------------\n");
}

int main(void){
	int serverB_fd;
    int numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];

    /*hardcoding server B UDP socket address*/
    struct sockaddr_in serverB;
	serverB.sin_family = AF_INET;
	serverB.sin_port=htons(SERVER_B_PORT);
	serverB.sin_addr.s_addr=inet_addr(localhostIP);
    memset(serverB.sin_zero, '\0', sizeof serverB);

    if ((serverB_fd = socket(PF_INET, SOCK_DGRAM,
                getprotobyname("udp")->p_proto)) == -1) {
        perror("serverB: socket");
        exit(1);
    }

    if (bind(serverB_fd, (struct sockaddr *) &serverB, sizeof serverB) == -1) {
        close(serverB_fd);
        perror("serverB: bind");
        exit(1);
    }

    //printf("listener: waiting to recvfrom...\n");
    printf("The Server B is up and running using UDP on "
    	"port %d.\n", ntohs(serverB.sin_port));

    while(1){
	    addr_len = sizeof their_addr;
	    if ((numbytes = recvfrom(serverB_fd, buf, MAXBUFLEN-1 , 0,
	        (struct sockaddr *)&their_addr, &addr_len)) == -1) {
	        perror("serverB: recvfrom");
	        exit(1);
	    }

	    buf[numbytes] = '\0';

	    /*
	    printf("listener: got packet from %s\n",
	        inet_ntop(their_addr.ss_family,
	            get_in_addr((struct sockaddr *)&their_addr),
	            s, sizeof s));
	    printf("listener: packet is %d bytes long\n", numbytes);
	    printf("listener: packet contains \"%s\"\n", buf);
	    */

	    /*PLAN: after receiving the message, parse it*/
	    char * messageFromAWS=buf;

	    double propSpeed, transSpeed, fileSize;
	    map<int,int> dist;
	    parseMessageFromAWS(messageFromAWS, &propSpeed, &transSpeed,
	    	&fileSize, &dist);
	    printParsedMessage(&propSpeed, &transSpeed,
	    	&fileSize, &dist);
	    /*PLAN: use the parsed message to calculate the results*/

	    /*PLAN: package the results*/
	    string messageToAWS=calculateDelays(&propSpeed, &transSpeed,
	    	&fileSize, &dist);

	    char * duplicateMessage=strdup(messageToAWS.c_str());
	    printTotalDelay(duplicateMessage);
	    /*PLAN: send the results*/

	    const char * testmess="B reporting!\0";
	    //printf("Down here!\n");
	    if ((numbytes = sendto(serverB_fd, messageToAWS.c_str(),
	    strlen(messageToAWS.c_str()), 0,
             (struct sockaddr *)&their_addr, addr_len)) == -1) 
	    {
	        perror("talker: sendto");
	        exit(1);
    	}
    	printf("The Server B has finished sending the output to AWS\n");

	}

    close(serverB_fd);
return 0;
}
