#include "warmsocks.h"
#include "dijkstra.cpp"
#include <fstream>
#include <iostream>
#include <sstream>

#define MAXGRAPHSIZE 12

void printMapConstructionResults(map<char, Graph*>* IDToGraph)
{
	/* PLAN: print header*/
	printf("The Server A has constructed a list of %d maps:\n", IDToGraph->size());
	printf("---------------------------------\n");
	printf("Map ID\tNum Vertices\tNum Edges\n");
	printf("---------------------------------\n");
	/* PLAN: iterate over the IDtoGraph map*/
	map<char, Graph*>::iterator it;
	for(it = IDToGraph->begin(); it!=IDToGraph->end(); ++it)
	{
		char mapID=it->first;
		Graph * graphP=it->second;
		printf("%c",mapID);
		printf("\t\t%d",graphP->uniqueVertices.size());
		printf("\t\t%d\n",graphP->numEdges);
	}
}

void printShortestPathResults(map<int, int> *distP)
{
	/* PLAN: print header*/
	printf("The Server A has identified the following shortest paths:\n");
	printf("---------------------------------\n");
	printf("Destination\tMin Length\n");
	printf("---------------------------------\n");
	/* PLAN: iterate over the IDtoGraph map*/
	map<int, int>::iterator it;
	for(it = distP->begin(); it!=distP->end(); ++it)
	{
		int vertex=it->first;
		int distance=it->second;
		printf("%d\t\t%d\n",vertex,distance);
	}
}

void createMessageToAWS(string * messageToAWS, Graph * chosenGraph,
	map<int,int>* distP)
{
	/*PLAN: attach the propagation and transmission speeds first*/
	string propStart (propHead);
	string propEnd (chosenGraph->propagationSpeed);
	string propString=propStart+intralim+propEnd;

	string transStart (transHead);
	string transEnd (chosenGraph->transmissionSpeed);
	string transString=transStart+intralim+transEnd;

	*messageToAWS=propString+interlim+transString;

	/*PLAN: loop over the map and attach vertex indices and
	their distances*/

	map<int, int>::iterator it;
	for(it = distP->begin(); it!=distP->end(); ++it)
	{
		ostringstream strToInt1; //this is mislabeled should be intToStr
		strToInt1<<it->first;
		string vertex=strToInt1.str();
		ostringstream strToInt2;
		strToInt2<<it->second;
		string distance=strToInt2.str();
		*messageToAWS=*messageToAWS+interlim+vertex+intralim+distance;
	}
}

int mapConstruction(const char * inputFilename, map<char, Graph*>* IDToGraph)
{
	Graph * newGraph;
	int lineCounter=0;
	const char * delim= " ";
	/* PLAN: open file with given filename for reading*/
	ifstream mapFile (inputFilename);
	if(mapFile.is_open())
	{
		string line;
		while(getline(mapFile, line))
		{
			char * duplicateLine=strdup(line.c_str());
			//printf("Line: %s\n",duplicateLine);
			char * firstToken=strtok(duplicateLine, delim);
			//printf("First token:%s\n",firstToken);
			if((atof(firstToken)==0) and not (strcmp(firstToken, "0")==0)) //the first token is a map ID
			{
				lineCounter=0;
				/* PLAN: make new graph with new map ID"*/
				char * newMapID=firstToken;
				newGraph=new Graph(MAXGRAPHSIZE);
				IDToGraph->insert(make_pair(*newMapID, newGraph));
			}else{ //the first token could be a speed or a vertex
				char * nextToken=strtok(NULL, delim);
				if (nextToken==NULL) //first token is a speed
				{
					/* I'm going to keep these speeds as char *,
					for convenience*/
					if(lineCounter==1)
					{
						char * propagationSpeed=firstToken;
						newGraph->propagationSpeed=propagationSpeed;
					}
					if(lineCounter==2)
					{
						char * transmissionSpeed=firstToken;
						newGraph->transmissionSpeed=transmissionSpeed;
					}
				}else{ //the first token is a vertex
					//printf("Next token:%s\n", nextToken);
					int firstVertex=atoi(firstToken);
					int secondVertex=atoi(nextToken);
					newGraph->uniqueVertices.insert(firstVertex);
					newGraph->uniqueVertices.insert(secondVertex);
					char * lastToken=strtok(NULL, delim);
					int weight=atoi(lastToken);
					newGraph->addEdge(firstVertex, secondVertex, weight);
					newGraph->numEdges++;
					/*
					printf("The first vertex is %d\n",firstVertex);
					printf("The second vertex is %d\n",secondVertex);
					printf("The weight is %d\n", weight);
					printf("The total number of edges is %d\n", newGraph->numEdges);
					printf("Number of unique vertices seen so far: %d\n",newGraph->uniqueVertices.size());
					*/
				}
			}
			lineCounter++;
		}
	}
	mapFile.close();
	return 0;
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(void){
	//variable declaration and arbitrary constants
    int serverA_fd;
    int numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];
    const char * mapFilename="map.txt";

    /* PLAN: get address info for self*/
    /*hardcoding server A UDP socket address*/
    struct sockaddr_in serverA;
	serverA.sin_family = AF_INET;
	serverA.sin_port=htons(SERVER_A_PORT);
	serverA.sin_addr.s_addr=inet_addr(localhostIP);
    memset(serverA.sin_zero, '\0', sizeof serverA);

    /* PLAN: create UDP socket*/
    if ((serverA_fd = socket(PF_INET, SOCK_DGRAM,
                getprotobyname("udp")->p_proto)) == -1) {
        perror("serverA: socket");
        exit(1);
    }

    /* PLAN: bind socket to server A port*/
    if (bind(serverA_fd, (struct sockaddr *) &serverA, sizeof serverA) == -1) {
        close(serverA_fd);
        perror("serverA: bind");
        exit(1);
    }

    //printf("listener: waiting to recvfrom...\n");
    printf("The Server A is up and running using UDP on port %d.\n",SERVER_A_PORT);


    /* PLAN: construct maps based on map.txt*/
    map<char, Graph*>* IDToGraph= new map<char, Graph*>;
    mapConstruction(mapFilename, IDToGraph);
    printMapConstructionResults(IDToGraph);

    /* PLAN: enter infinite loop*/
    while(1){
    	/* PLAN: receive (i.e. wait for) a message*/
	    addr_len = sizeof their_addr;
	    if ((numbytes = recvfrom(serverA_fd, buf, MAXBUFLEN-1 , 0,
	        (struct sockaddr *)&their_addr, &addr_len)) == -1) {
	        perror("recvfrom");
	        exit(1);
	    }
	    /* PLAN: when a message arrives, parse it*/

	    buf[numbytes] = '\0';

	    /*
	    printf("listener: got packet from %s\n",
	        inet_ntop(their_addr.ss_family,
	            get_in_addr((struct sockaddr *)&their_addr),
	            s, sizeof s));
	    printf("listener: packet is %d bytes long\n", numbytes);
	    printf("listener: packet contains \"%s\"\n", buf);*/

	    const char * delimFromAWS=",";
	    char * mapID=strtok(buf, delimFromAWS);
	    int sourceVertexIndex=atoi(strtok(NULL, delimFromAWS));

	    printf("The Server A has received input for finding shortest paths: ");
	    printf("starting vertex %d of map %s.\n", sourceVertexIndex, mapID);

	    /* PLAN: feed this parsed message into Dijkstra's algorithm*/
	    Graph * chosenGraph=(*IDToGraph)[*mapID];
	    map<int, int> dist=chosenGraph->shortestPath(sourceVertexIndex);
	    dist.erase(sourceVertexIndex); //remove the source vertex from the results
	    printShortestPathResults(&dist);



		/* PLAN: package the output*/
		string messageToAWS;
		createMessageToAWS(&messageToAWS, chosenGraph, &dist);

		/* PLAN: send the output to the same address that we received the message from*/

	    const char * testmess="Yolololo!\0";

	    if ((numbytes = sendto(serverA_fd, messageToAWS.c_str(),
	    	strlen(messageToAWS.c_str()), 0,
             (struct sockaddr *)&their_addr, addr_len)) == -1) {
	        perror("talker: sendto");
	        exit(1);
    	}

    	printf("The Server A has sent shortest paths to AWS.\n");

	}

    close(serverA_fd);

    return 0;
}
